function check() {
  function doesPinContain4Digits(input) {
    if (!input || input.length != 4) {
      return false;
    }
    for (let i = 0; i < input.length; i++) {
      const digit = parseInt(input[i]);
      if (!digit && digit !== 0) {
        return false;
      }
    }
    return true;
  }

  const guessedPinStr = document.getElementById('pin-code').value;
  const elOutput = document.getElementById('output-check');
  if (doesPinContain4Digits(guessedPinStr)) {
    /*
     * Ihre Implementation hier
     *
     * Ermitteln Sie,
     * - wie viele Ziffern des eingegebenen Codes korrekt sind
     * - ob die korrekten Ziffern an der richtigen Position im Code stehen oder nicht
     * Falls der Code noch nicht erraten wurde:
     * - Schreiben Sie den entsprechenden  Hinweis in das p-Element für den Output (id output-check)
     * Falls der Code erraten wurde:
     * - Rufen Sie die Funktion gameWon() auf
     */
  } else {
    // Eingabe nicht korrekt
    elOutput.textContent =
      'Bitte PIN Code eingeben, der sich aus 4 Ziffern zusammensetzt.';
  }
}

function gameWon() {
  function disableInputElements() {
    const elPinInput = document.getElementById('pin-code');
    elPinInput.setAttribute('disabled', '');
    const elGuessBtn = document.getElementById('btn-guess');
    elGuessBtn.setAttribute('disabled', '');
  }

  function addCongrats() {
    const elOutput = document.getElementById('output-check');
    elOutput.textContent = 'Bravo! PIN Code erraten.';
  }

  function addButtonToRestart() {
    const elBtnRestart = document.createElement('button');
    elBtnRestart.setAttribute('id', 'btn-restart');
    elBtnRestart.setAttribute('onclick', 'init()');
    elBtnRestart.textContent = 'Neu starten';
    const elBody = document.querySelector('body');
    const elScript = elBody.querySelector('script');
    elBody.insertBefore(elBtnRestart, elScript);
  }

  disableInputElements();
  addCongrats();
  addButtonToRestart();
}

function init() {
  function setRandomPin() {
    let digits = [];
    for (let i = 0; i < 4; i++) {
      const digit = parseInt(Math.random() * 10);
      digits.push('' + digit);
    }
    pin = digits;
  }

  function enableInputElements() {
    const elPinInput = document.getElementById('pin-code');
    elPinInput.value = '';
    elPinInput.removeAttribute('disabled');
    const elGuessBtn = document.getElementById('btn-guess');
    elGuessBtn.removeAttribute('disabled');
  }

  function clearOutputAndRemoveBtnToRestart() {
    const elBtnRestart = document.getElementById('btn-restart');
    if (elBtnRestart) {
      elBtnRestart.remove();
    }
    const elOutput = document.getElementById('output-check');
    elOutput.innerHTML = '';
  }

  setRandomPin();
  enableInputElements();
  clearOutputAndRemoveBtnToRestart();
}

let pin;
init();
